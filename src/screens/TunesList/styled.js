/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import {ListItem, View, Button, Text} from 'native-base';



export const ButtonView = styled(Button)`
  background-color: #FF9501;
`;

export const ButtonText = styled(Text)`
  padding: 10px;
`;


export const SeparatorView = styled(View)`
  background-color: #080808;
  height: 0.3px;
  width: 100%;
`;
