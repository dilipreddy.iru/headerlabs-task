/* eslint-disable prettier/prettier */
/*This is an Example of SearchBar in React Native*/
import React  from "react";
import {
  View,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  Platform,
  TouchableOpacity,
  Modal,
  Linking,
} from "react-native";

import { WebView } from "react-native-webview";
import { SearchBar } from "react-native-elements";
import { Left, Right, Body } from "native-base";
import { ButtonView, ButtonText, SeparatorView } from "./styled";

export default class TunesList extends React.Component {
  constructor(props) {
    super(props);
    //setting default state
    this.state = { isLoading: true, search: "", isOpen: false, modalData: "" };
    this.arrayTunes = [];
  }

  componentDidMount() {
    return fetch("https://itunes.apple.com/us/rss/topalbums/limit=100/json")
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson.feed.entry);

        this.setState(
          {
            isLoading: false,
            dataSource: responseJson.feed.entry,
          },
          function () {
            this.arrayTunes = responseJson.feed.entry;
          }
        );
      })
      .catch((error) => {
        console.error(error);
      });
  }

  search = (text) => {
    console.log(text);
  };
  clear = () => {
    this.search.clear();
  };

  openPopup = (id) => {
    Linking.openURL(id).catch((err) => console.error("An error occurred", err));
    // this.setState({
    //   isOpen: true,
    //   modalData: id,
    // });
  };

  doSortbyDate = () => {
    this.arrayTunes.sort(function (a, b) {
      return (
        new Date(a["im:releaseDate"].label) -
        new Date(b["im:releaseDate"].label)
      );
    });
  };

  priceFilter = (vals) => {
    return vals.sort((a, b) => {
      const aPrice =
        a["im:price"].label[0] === "$"
          ? parseFloat(a["im:price"].label.slice(1, -1))
          : 0;
      const bPrice =
        b["im:price"].label[0] === "$"
          ? parseFloat(b["im:price"].label.slice(1, -1))
          : 0;
      return aPrice - bPrice;
    });
  };

  doSortbyName = (artists) => {
    artists.sort(function (a, b) {
      if (a["im:artist"].label < b["im:artist"].label) {
        return -1;
      }
      if (a["im:artist"].label > b["im:artist"].label) {
        return 1;
      }
      return 0;
    });
  };

  SearchFilterFunction(text) {
    const newData = this.arrayTunes.filter(function (item) {
      const itemData = item["im:name"].label
        ? item["im:name"].label.toUpperCase()
        : "".toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });

    this.setState({
      dataSource: newData,
      search: text,
    });
  }

  ListViewItemSeparator = () => {
    //Item sparator view
    return <SeparatorView />;
  };

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      //ListView to show with textinput used as search bar
      <View style={styles.viewStyle}>
        <SearchBar
          round
          // searchIcon={{ size: 24 }}
          onChangeText={(text) => this.SearchFilterFunction(text)}
          onClear={(text) => this.SearchFilterFunction("")}
          placeholder="Type Here..."
          value={this.state.search}
        />

        <View style={styles.container}>
          <View style={styles.button}>
            <ButtonView onPress={() => this.doSortbyDate()}>
              <ButtonText>Sort by Date</ButtonText>
            </ButtonView>
          </View>
          <View style={styles.button}>
            <ButtonView onPress={() => this.priceFilter(this.arrayTunes)}>
              <ButtonText>Sort by Price</ButtonText>
            </ButtonView>
          </View>
          <View style={styles.button}>
            <ButtonView onPress={() => this.doSortbyName(this.arrayTunes)}>
              <ButtonText>Sort by Name</ButtonText>
            </ButtonView>
          </View>
        </View>

        <FlatList
          data={this.state.dataSource}
          ItemSeparatorComponent={this.ListViewItemSeparator}
          //Item Separator View
          renderItem={({ item }) => (
            <View>
              <TouchableOpacity onPress={() => this.openPopup(item.id.label)}>
                <Left>
                  <ButtonText>{item["im:name"].label}</ButtonText>
                </Left>
                <Body>
                  <ButtonText>{item["im:price"].label}</ButtonText>
                </Body>
                <Right>
                  <ButtonText>{item["im:releaseDate"].label}</ButtonText>
                </Right>
              </TouchableOpacity>
            </View>
          )}
          enableEmptySections={true}
          style={{ marginTop: 10 }}
          keyExtractor={(item, index) => index.toString()}
        />
        {/* {
          this.state.isOpen && (
            // <Modal isOpen={this.state.isOpen} onRequestClose={() => {
            //  this.setState({isOpen: false});
            // }}>
            //   <WebView
            //   source={{ html:   '<iframe width="100%" height="50%" src="https://music.apple.com/us/album/final-fantasy-xiv-shadowbringers-ep2/1529840143?uo=2" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> '}}
            //     style={{ marginTop: 20 }}
            //   />
            // </Modal>
          )
        } */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    // justifyContent: 'center',
    // flex: 1,
    backgroundColor: "white",
    marginTop: Platform.OS == "ios" ? 30 : 0,
  },
  container: {
    padding: 30,
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  button: {
    width: "35%",
    height: 40,
  },
});
